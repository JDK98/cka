# Access Pod Infos inside the container


Liens importants : https://kubernetes.io/docs/tasks/inject-data-application/environment-variable-expose-pod-information/#the-downward-api

### Using Env

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
      - name: sidecar-busybox
        image: busybox
        command: ["sh","-c"]
        args: 
        - while true; do
            echo -en '\n';
            printenv MY_NODE_NAME MY_POD_NAME MY_POD_IP;
            sleep 10;
          done;
        env: 
          - name: MY_NODE_NAME
            valueFrom:
              fieldRef: 
                fieldPath:  spec.nodeName 
          - name: MY_POD_NAME
            valueFrom:
              fieldRef: 
                fieldPath:  metadata.name
          - name: MY_POD_IP
            valueFrom:
              fieldRef: 
                fieldPath:  status.podIP
```

