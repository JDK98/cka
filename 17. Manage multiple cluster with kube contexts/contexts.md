# Contexts

```yaml
#List all the contexts
kubectl config get-contexts 

#List the current context
kubectl config current-context
```