# Sandbox

Sandbox add a security layer to reduce attack surface, there is no direct access to the hardware 

It's better for small containers and require more resources

It's not good for syscall heavy workload 

![SandBox](../00.images/sandbox.png)

## Open Container Initiative (OCI)

Linux foundation project to define open standard for virtualisation

Define specification of runtime, image, distribution

Create runtime that implement their specification : runc

## CRI

Container Runtime Interface allow Kubelet to communicate with different Container Runtime

#### Before CRI 

![Before CRI](../00.images/before%20CRI.png)

#### After CRI

![With CRI](../00.images/with%20CRI.png)


## KataContainers

CRI Sandbox

- Provides Strong separation layer
- Run every container in its own private VM
- Use virtualisation and in modern cloud it must be difficult to set it 

![Katacontainer](../00.images/katacontainers.png)

## Gvisor

- User-space kernel for containers
- Another layer of separation
- Not hypervisor / VM based
- Simulate kernel syscall with limited functionality
- Runs in userspace separated from linux kernel
- runtime's called runsc

![Gvisor](../00.images/gvisor.png)