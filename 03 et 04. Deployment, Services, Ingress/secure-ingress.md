## Let's secure ingress 

![LAB secure ingress](../00.images/secure-ingress.png)

1. Create & Expose pod1, pod2

```yaml
# Create pod1 and expose it with a service named pod1
k run pod1 --image=nginx --expose=true --port=80

# Create pod2 and expose it with a service named pod2
k run pod2 --image=httpd --expose=true --port=80
```

2. Create Ingress controller

💡 It can be installed using helm (Check helm documentation)

```yaml
kubectl apply -f https://raw.githubusercontent.com/killer-sh/cks-course-environment/master/course-content/cluster-setup/secure-ingress/nginx-ingress-controller.yaml

💡 It will create a NodePort service
```


3. Create Ingress Rule 

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: secure-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  ingressClassName: nginx
  rules:
  - http:
      paths:
      - path: /service1
        pathType: Prefix
        backend:
          service:
            name: pod1
            port:
              number: 80
      - path: /service2
        pathType: Prefix
        backend:
          service:
            name: pod2
            port:
              number: 80
```

4. Test

Get The IP address of the Node and the port 80/443 of the Ingress Nginx Controller

```yaml
curl http://34.249.151.214:30881/service1

curl http://34.249.151.214:30881/service2

#Will result in an error, use the -k to accept server sign certificate
curl https://34.249.151.214:31562/service2 -kv

curl https://34.249.151.214:31562/service1 -kv
```

5. Create Self Signed certificate 

```yaml
# Create certificate
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes
```

6. Create a secret

```yaml
k create secret tls secure-ingress --cert=/home/ubuntu/secure-ingress/cert.pem --key=/home/ubuntu/secure-ingress/key.pem
```

7. Use the secret in ingress rule

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: secure-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  ingressClassName: nginx
  # Add
  tls:
  - hosts:
      - secure-ingress.com #CN specify in the create certificate command
    secretName: secure-ingress # Name of the secret
  #
  rules:
  - host: secure-ingress.com #Add
    http:
      paths:
      - path: /service1
        pathType: Prefix
        backend:
          service:
            name: pod1-secure
            port:
              number: 80
      - path: /service2
        pathType: Prefix
        backend:
          service:
            name: pod2-secure
            port:
              number: 80
```

8. Test with the certiifcate 

```yaml
curl https://secure-ingress.com:31562/service1 -kv --resolve secure-ingress.com:31562:34.249.151.214
```

OR

```yaml
#Add in /etc/hosts
34.249.151.214 secure-ingress.com

curl https://secure-ingress.com:31562/service1 -kv
```