# Create a ingress controller to access internal service on aws

### LAB

    Create One Pod running nginx image only accessed within the cluster
    Create an ingress rule to make the application accessible from internet

### SOLUTION

1. Create internal service and pod 

````
kubectl run nginx --image=nginx --expose=true --port=80 --dry-run=client -o yaml > nginx.yaml
kubectl apply -f nginx.yaml
````

2. Install helm ([Helm Documentation](https://helm.sh/docs/) )

````
sudo apt-get install helm
helm version
````

3. Deploy ingress Controller in the default namespace

````
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm upgrade --install ingress-nginx ingress-nginx
 💡 If we want to create in different namespace and create this namespace from command, add   `--namespace ingress-nginx --create-namespace` 
The ingress controller will create a LoadBalancer and ClusterIp Service   
````

4. Create An AWS LoadBalancer to interface the LoadBalancer create by Ingress controller 

5. Configure the ingress rule 

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-wildcard-host
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
    kubernetes.io/ingress.class: "nginx"
spec:
  rules:
  - host: nginx-ingress-lb-1861941969.eu-west-1.elb.amazonaws.com  # dns name of the ALB created at step 4
    http:
      paths:
      - pathType: Exact
        path: "/"
        backend:
          service:
            name: nginx
            port:
              number: 80
```

6. Test