# Certificate

1. Certificate Authority ---> /etc/kubernetes/pki/ca.crt

2. API Server cert --->  /etc/kubernetes/pki/apiserver.crt

3. Etcd Server Cert ---> /etc/kubernetes/pki/etcd/server.crt

4. Api -> etcd ---> /etc/kubernetes/pki/apiserver-etcd-client.crt

5. Api -> Kubelet ---> /etc/kubernetes/pki/apiserver-kubelet-client.crt

6. Scheduler -> Api ---> /etc/kubernetes/scheduler.conf

7. Controller-manager -> Api ---> /etc/kubernetes/controller-manager.conf

8. Kubelet -> Api ---> /etc/kubernetes/kubelet.conf

9. Kubelet Server Cert ---> /var/lib/kubelet/pki/kubelet.crt