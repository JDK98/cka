# KUBERNETES SECURITY

## 1.Host Operating System Security 

-   K8S Nodes should only do one thing : K8s
-   Reduce Attack surface ( remove unnecessary app, keep up to date)
-   Find & Identify malicious process 
-   Use Runtime security tools
-   Restrict IAM / SSH Access 

## 2. Kubernetes Cluster Security

-   Ensure that K8s component are up to date (apiserver, kubelet, etcd...)
-   Restrict External Access
-   use Authentication, Authorization and Admission Controllers
-   Enable Audit Logging
-   Use Security Benchmarking like CIS

## 3. Application Security

-   Use Secrets / No hardcoded value
-   Use RBAC
-   Container Sandboxing
-   Container Hardening (Reduce Attack Surface, Run as User, RO Filesystem)
-   Vulnerability Scanning
-   Mtls / Service Mesh


# Certificate Authority

Located at `/etc/kubernetes/pki/ca.crt` it is the trusted root of all certificates in the cluster

All Cluster certificate are signed by CA

He is used by components to validate each other

💡 Etcd has its own CA certificate 