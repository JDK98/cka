# Troubleshooting


### Is pod running ?

```yaml
kubectl get pod POD_NAME
```

### Is pod registered with service ?

```yaml
kubectl get ep

kubectl describe service SERVICE_NAME
````

### Is service accessible ?

```yaml
# Create temporary pod for testing
kubectl run tmp --restart=Never --rm -i -- curl <serviceName | Ip>:<port>

kubectl run tmp --restart=Never --rm -i -- ping SERVICE_NAME

kubectl run tmp --restart=Never --rm -i -- curl -m 5 10.101.74.24:5000 -w "\n"

kubectl run tmp --restart=Never --rm -i -- wget -O- -t 5 10.101.74.24:5000
```

```yaml
# Exec in pod for testing
kubectl exec tmp -it -- nc -v -z -w 2 SERVICE_IP/SERVICE_NAME SERVICE_PORT
```

### Check app logs 

```yaml
kubectl logs POD_NAME
```

### Check pod status and recent events

```yaml
kubectl describe pod POD_NAME
```
