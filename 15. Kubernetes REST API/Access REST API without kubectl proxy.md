# Access REST API without Kubectl proxy


1. Create Service account

2. Create Role and RoleBinding

3. Get the token 

```yaml
kubectl describe secret <secret>
```

4. Get the server endpoint 

    kubectl connfig view 

5. 

    curl -X GET $server/api --header "Authorization: Bearer $token" --cacert /etc/kubernetes/pki/ca.crt

    curl -X GET $server/api/v1/namespaces/default/services --header "Authorization: Bearer $token" --cacert /etc/kubernetes/pki/ca.crt

OR  

    curl -X GET $server/api/v1/namespaces/default/services --header "Authorization: Bearer $token" --insecure


Liens importants : https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.24/#-strong-api-overview-strong-

```yaml
# Get List of deployments
curl -X GET $server/apis/apps/v1/namespaces/default/deployments --header "Authorization: Bearer $token" --cacert /etc/kubernetes/pki/ca.crt
``` 
```yaml
# Get a specific deployment 
curl -X GET $server/apis/apps/v1/namespaces/default/deployments/myapp-resources --header "Authorization: Bearer $token" --insecure
```