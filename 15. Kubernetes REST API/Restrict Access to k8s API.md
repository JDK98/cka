# Restrict Access To Kubernetes API

![Request Workflow](../00.images/request%20workflow.png)

## Restrictions

- Don't allow anonymous access
- Close insecure port
- Don't expose Apiserver outside
- Restrict Access to Node (NodeRestriction)
- Prevent unauthorized RBAC
- Prevent pods accessing API (automountServiceAccountToken: false)
- APIServer port shoud be behind firewall / allowed Ip range on cloud platform

## Anonymous Access

Anonymous Access is enables by default since version 1.6 if authorization mode other than Always if configure  

ABAC and RBAC require explicit authorization for anonymous access

```yaml
# Add in /etc/kubernetes/manifest/kube-apiserver.yaml
--anonymous-auth=true|false
```

## Insecure Access 

Allow to send HTTP request to the Apiserver and bypass the authentication and authorization modules 

Admission Controller modules stills enforces 

🚨 Only use for debugging purpose 

```yaml
# Add in /etc/kubernetes/manifest/kube-apiserver.yaml
--insecure-port=<port> # 0 means it's disabled

# Test
curl http://localhost:<port>
```

## Manual API Request 

````yaml
kubectl config view --raw

# Decode64 (base64 -d) the ca, cert and key section and store it in file randomly named ca, cert, key

curl https://localhost:6443 --ca ca --cert cert --key key 
````

## Access the API Server locally

1. Change the default Kubernetes service to NodePort 

2. Copy the Kubernetes Cluster Config locally

````yaml
# Copy the output in a file locally, let's call it conf
k config view --raw
````

3. In the conf file change the server endpoint to https://<server-ip>:<nodeport-port>
   💡 Open the port in Security group

4. Test 

````yaml
kubectl --kubeconfig conf get ns

💡 There will be an error because the api certificate accept a specific IP Address, check it by :

openssl x509 -in apiserver.crt -text

In the conf file change the server part with the DNS Name specified in the openssl output

In /etc/hosts, record the DNS Name with the Ip address

TEST AGAIN
````

## NodeRestriction