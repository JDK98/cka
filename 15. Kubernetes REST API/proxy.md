# Access API with Kubectl proxy

It Creates a proxy server between localhost and the Kubernetes Api server  
Use connection as configured in the kubeconfig  
Allow to Access API Locally just over http and without authentication 

![Kubectl proxy](../00.images/k%20proxy.png)

```yaml
kubectl proxy --port=8080 &

curl http://127.0.0.1:8080

curl http://127.0.0.1:8080/api/v1/namespaces/

curl http://127.0.0.1:8080/api/v1/namespaces/default/

curl http://127.0.0.1:8080/api/v1/namespaces/default/services

```

