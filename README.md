# ALL NOTES ABOUT KUBESTRONAUT

KCNA + KCSA + CKAD + CKA + CKS

## Some interesting Command

````yaml
# Create a container c2 in the same namespace as container c1
sudo docker container run -d --name c2 --pid=container:c1 ubuntu sh -c "sleep 2d"

#Initialise le cluster 
kubeadm init

#Affiche des infos sur le cluster
kubectl cluster-info

#Affiche des infos sur les différents composants kubernetes (très utile) 
kubectl api-resources
kubectl api-resources --namespaced=true|false
```


nc -v -z -w 2 svc-name port

wget -O- -t 5 10.101.74.24:5000

curl -m 5 10.101.74.24:5000 -w "\n" 

nslookup google.com (check for DNS 53)

k run temp-pod --image=nginx --restart=Never --rm -i -- curl app1.app.svc.cluster.local:80


k run pod --image=busybox --dry-run=client -o yaml > pod.yaml -- sh -c "sleep 1d"


`kubectl describe <component> <nom>` = Affiche des informations sur le composant 

`kubectl get ep` = Affiche les endpoints du cluster 

`kubectl get all` = Affiche tous les composants dans le namespace par défaut 

`kubectl get pods --all-namespaces` = Affiche tous les composants dans tous les namespaces 

`kubectl edit <component> <nom>` = Affiche le fichier de configuration du composant

`kubectl get <component> <nom> -o yaml` = Affiche les informations du composant au format yaml 

`kubectl get <component> <nom> --show-labels` = Affiche les infos avec une colonne labels 

`kubectl get <component> -l <label>` = Affiche les composants avec le labels précisés

`kubectl scale deployment <name> --replicas=04` = Scale up/down le nombre de replicas 

`kubectl scale deployment <name> --replicas=02 --record` = Enregistre le changement

`kubectl rollout history <component>/<name>` = Liste les enregistrements   

`kubectl run <name> --image=<image>` = Créer un pod 

`kubectl exec -it nginx-deployment-7848d4b86f-brtxk -- bash` = Ouvrir un terminal à l'intérieur du pod (si le pod n'a qu'un seul conteneur) 

`kubectl exec -it pod -- sh -c "echo hello"` = Exécuter une commande depuis le pod sans y accéder 

`kubectl create service clusterip test-svc --tcp=8080:8080 --dry-run=client -o yaml > test-svc.yaml` = Permet de générer un fichier de configuration (Retrouvez les autres composants en suivant `kubectl <composant> --help`| Pour les pods `kubectl run --help `)

`journalctl -u kubelet` = Affiche les logs du srevices 

`k get pod -o jsonpath="{.items[*].metadata.name}"` = Affiche les noms de tous les pods (range to list on separate line)

`k get pod -o jsonpath="{.items[*].spec.containers[0].image}"` = Affiche les images des pods (range to list on separate line)

`jsonpath and custom column`


`kubectl rollout restart deployment/mydb` = Restart tous les pods du deployment mydb. Très utile après une modif du configmap/secret monté sur les pods 

`kubectl -n kube-system rollout restart deployment coredns`

`kubectl run mypod --image=nginx` = Créer un pod 

`kubectl create deployment nginx-deployment --image=nginx` = Créer un déploiement avec image nginx

`kubectl create deployment nginx-deployment --image=nginx --replicas=2` = Créer un déploiement avec image nginx et deux replicas

`kubectl expose deployment nginx-deployment --type=NodePort --name=nginx-service` =  Créer un service pour le déploiement

`kubectl create --help ` = Infos à propos des options de la commande 

`kubectl create service clusterip myservice --tcp=80:80 --dry-run=client -o yaml > myservice.yaml` =  Créer un service de type clusterIp nommé myservice écoutant sur le port 80 avec des conteneurs ecoutants sur le 80

`alias kube=kubectl`

export do="--dry-run=client -o yaml"

`kubectl create service clusterip myservice --tcp=80:80 $do > myservice.yaml`

`kubectl scale --replicas=3 deployments/mysql` = Scale to 3 replicas
