# CIS Benchmark

- Center of Interest Security

- Define best practices for the secure configuration of a target system

- Covering more than 14 Tech

- Provide default security rules 

- Check and download the CIS benchmark documentation according to your use case  

- Use Kube-bench to scan the cluster and get the remediation in the command output OR in the CIS Benchmark pdf

```yaml
sudo docker run --pid=host -v /etc:/etc:ro -v /var:/var:ro -t docker.io/aquasec/kube-bench:latest --version 1.18
```