# Upgrade cluster 

Liens important: https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/

```yaml
1.9.2

1 : Major
9 : Minor 
2 : Patch

Minor version every 3 month
```

First update the master component (apiserver, scheduler...)  
Then update the worker component (kubelet, kube-proxy...)  

Component has to have same minor version as apiserver or one version below 
