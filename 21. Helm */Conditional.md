# Conditional

#### Task : 

```
Deploy an HPA resource for releases in the prod-ns namespace.
Do not deploy an HPA resource for releases in any other namespace
Deploy a mock-app-prod release in prod-ns namespace and a mock-app-dev release in dev-ns to check the solution.
```

#### Solutions

`vim /charts/mock-app/templates/hpa.yaml`

````yaml 
{{- if eq .Release.Namespace "prod-ns" }}
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
  name: {{ .Values.appName }}-hpa
  labels:
    app: {{ .Values.appName }}
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: {{ .Values.appName }}-deployment
  minReplicas: 1
  maxReplicas: 3
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 50

{{- end }}
````

`helm install -n dev-ns mock-app-dev /charts/mock/app/.`

`helm install -n prod-ns mock-app-prod /charts/mock/app/.`


