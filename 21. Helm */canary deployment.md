# Canary Deployment 

[Script à déplacer]
```sh
export PORT=5000
export SERVICE_IP=$(kubectl get svc -n prod-ns -l app=mock-app -o jsonpath='{.items[0].spec.clusterIP}')
for ((i=1; i<=20; i++)); do
    # Execute curl command and print newline
    curl -s "http://${SERVICE_IP}:${PORT}" -w "\n"
done
```

Canary deployment is a progressive deployment strategy that allows for the gradual release of a new version of an application to a subset of users before rolling it out to the entire production environment.   This approach minimizes the risk associated with introducing new software versions by first exposing a small percentage of users to the changes.   If the new version performs well and meets the necessary criteria, it is gradually scaled up to more users until it fully replaces the old version.   Key benefits of canary deployment include reduced risk of widespread issues, the ability to monitor and validate new features in real-time, and the opportunity to roll back quickly if problems are detected.

TO DO