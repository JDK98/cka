# Automatic rollout of deployment 

Modify the message value of the configmap

`helm upgrade --install -n dev-ns mock-app /charts/mock-app/. --set message="You are overriding the message. Does the pod take this change in consideration ?"`

When a Helm chart is upgraded and the ConfigMap it relies on is updated, the pod's environment variables, set during pod creation, remain unchanged.   
This means the pod doesn't automatically recognize the new ConfigMap data. To apply the changes, a manual pod restart or redeployment is required.


Manual rollout restart  

`k rollout restart -n dev-ns deployment mock-app-deployment`  


Automatic rollout restart  

`vim /charts/mock-app/templates/deployment.yaml`

````yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.appName }}-deployment
  labels:
    app: {{ .Values.appName }}
spec:
  replicas: 1 
  selector:
    matchLabels:
      app: {{ .Values.appName }}
  template:
    metadata:
      annotations:
        checksum/config: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}  # Add checksum/config annotations
      labels:
        app: {{ .Values.appName }}
    spec:
      containers:
        - name: {{ .Values.appName }}-container
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          ports:
          - containerPort: 5000
          envFrom:
          - configMapRef:
              name: {{ .Values.appName }}-configmap
````


