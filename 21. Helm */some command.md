# Some Command

Helm est un package manager pour Kubernetes  

On peut héberger et retrouvez des helm charts sur des repo sur le Helm Hub  

[Helm Documentation](https://helm.sh/docs/) 


----------------------

#### Add a repo :  
````
helm repo add <given name of the repo> <url of the repo>
----------------------------------
helm repo add bitnami https://charts.bitnami.com/bitnami
````

#### List all the repo :  
```yaml
helm repo ls
```  

#### List the chart in a local repo:  
```yaml
helm search repo <name of the repo>
-----------------------------------
helm search repo bitnami
```

#### Search for a specific chart in the repo : 
```yaml
helm search repo <name of the repo>/<name of the chart>
-----------------------------------
helm search repo bitnami/nginx
```

#### Create a release from a chart : 
```yaml
helm install <given name of the release> <name of the repo>/<name of the chart in the repo>
-----------------------------------
helm install my-nginx bitnami/nginx
```

#### List the release : 
```
`helm ls` : List release in the default namsepace  
`helm ls -n <namespace>` : List release in a specific namespace  
`helm ls -a -A` : List all the release (Running or Not) in all Namespace    
`helm ls -o yaml` : List the release in yaml format
```

#### Show values of a chart in a repo  : 
```
helm show values <name of the repo>/<name of the chart in the repo>
-----------------------------------
helm show values bitnami/nginx
```

#### Create a release from a chart and override values from the command line : 
```
helm install <given name of the release> <name of the repo>/<name of the chart in the repo> --set key1=value1 
-----------------------------------
helm install my-nginx bitnami/nginx --set replicaCount=2
```

#### Get all values set by user for a release : 
```
helm get values <release_name> -n <namespace> 
-----------------------------------
helm get values my-nginx
```

#### Get all values set by computer for a release : 
`helm get values --all <release_name> -n <namespace>`   

`helm get values --all my-nginx`

#### Upgrade a release : 
`helm upgrade --install -n <namespace> <name of the release> <name of the repo>/<name of the chart in the repo>`   

`helm upgrade --install -n dev-ns mock-app mock-app-repo/mock-app`

#### Get history of a release: 
`helm history -n <namespace> <name of the release>`   

`helm history -n dev-ns mock-app`. 

#### Rollback to a specific version

`helm rollback -n <namespace> <name of the release> <revision>`  

`helm rollback -n dev-ns mock-app 1` (Will create another revision)

#### Uninstall a release

`helm uninstall -n <namespace> <name of the release>`  

`helm uninstall -n dev-ns mock-app`

#### Get all file of a release

`helm get all -n dev-ns mock-app`

#### Override a values in the chart tgz from command line 

`helm upgrade --install -n dev-ns mock-app /charts/mock-app-1.0.0.tgz --set message="You are overriding the message using an inline value. Good job !"`

#### Override a values in the chart tgz from values file

Create values.yaml : `vim values.yaml`   
````yaml
message: "You rock !"
````

`helm upgrade --install -n dev-ns mock-app /charts/mock-app-1.0.0.tgz --values /charts/values.yaml`