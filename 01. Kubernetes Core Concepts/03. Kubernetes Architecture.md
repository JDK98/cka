# Kubernetes Architecture 

Dans l'architecture d'un cluster Kubernetes on remarque deux types de Nodes avec des composants et des rôles variants.

![Kubernetes Architecture](../00.images/k8s%20architecture.jpg)

## Worker Node
---------------

__Container Runtime__ : Il s'agit de la technologie chargé de gérer nos images. On a dans cette catégorie, Docker, cri-o, container-d. Container-d est aujourd'hui le runtime utilisé par Kubernetes.

__Kubelet__ : Communique avec l'Apiserver et permet d'interagir avec les Nodes et les Pods

__Kube-proxy__ : Permet la communication entre les différents pods et effectue une redirection intelligente.


## Master Node
---------------
__ApiServer__ : Point d'interaction avec le cluster via UI, CLI ou API. Il sert aussi à authentifier les requêtes.

__Scheduler__ : Planifie de façon intelligente le déploiement d'un nouveau pod sur un node. Il n'effectue que la planification, c'est le Kubelet qui se charge de réellement créer le pod. Il utilise une politique de filter scoring pour placer le pod sur le meilleur pod pour son execution. Il est possible d'avoir un custom scheduler pour appliquer des custom policies.

__Controller Manager__ : Détecte les changements d'etat du cluster et envoie des requetes au Scheduler afin de retrouver l'etat désiré(On parle de reconciliation).  Ex : Lorsqu'un pod crash, le controller manager envoie une requête de création d'un nouveau pod au scheduler qui planifie la création du nouveau pod puis kubelet l'execute.

__etcd__ : Il s'agit d'une base de donnees de type Key/value. Il enregistre les infos concernant le cluster. Il a son propre Certificate Authority 
