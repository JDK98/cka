# NodeRestriction

## LAB

        The Kubelet on node01 shouldn't be able to set Node labels : 
            - starting with node-restriction.kubernetes.io/*
            - on other Nodes than itself
        Verify this is not restricted atm by performing the following actions as the Kubelet from node01 :
          add label killercoda/one=123 to Node controlplane
          add label node-restriction.kubernetes.io/one=123 to Node node01

## SOLUTION

````yaml
ssh node01
export KUBECONFIG=/etc/kubernetes/kubelet.conf
k label nodes controlplane killercoda/one=123
k label nodes node01 node-restriction.kubernetes.io/one=123
````

## LAB

        Enable the NodeRestriction Admission Controller and verify the issue is gone by trying to
          add label killercoda/two=123 to Node controlplane
          add label node-restriction.kubernetes.io/two=123 to Node node01

## SOLUTION

### 1. Enable NodeRestriction

```yaml
# Add --enable-admission-plugins=NodeRestriction to kube-apiserver.yaml
vim /etc/kkubernetes/manifests/kube-apiserver.yaml
```

### 2. Labelling

```yaml
k label nodes controlplane killercoda/two=123 # Restrict

k label nodes node01 node-restriction.kubernetes.io/two=123 # Restrict

k label nodes node01 test=okay # Allow
```