# APISERVER CRASH

## LAB
        The idea here is to misconfigure the Apiserver in different ways, then check possible log locations for errors.
        You should be very comfortable with situations where the Apiserver is not coming back up.
        Configure the Apiserver manifest with a new argument --this-is-very-wrong.
        Check if the Pod comes back up and what logs this causes.
        Fix the Apiserver again.

## SOLUTION

### 1. Edit the kube-apiserver.yaml file 

```yaml
# Always make a copy
mkdir /etc/kubernetes/backup

cp /etc/kubernetes/manifests/kube-apiserver.yaml /etc/kubernetes/backup


# Add --this-is-very-wrong in the container command section 
vim /etc/kubernetes/manifests/kube-apiserver.yaml
```

##### The Kube-apiserver pod will not boot

<br>

### 2. Checks logs 
--------

#### - `/var/log/pods/...` Check Pods Logs 

#### - `/var/log/containers/...` Check Containers Logs

#### - `crictl ps` `crictl logs` If ContainerD is used as runtime Container 

#### - `docker ps` `docker logs` If Docker is used as runtime Container 

#### - `journalctl | grep apiserver`  

#### - `grep xxx /var/log/syslog` Kubelet Logs

<br>

### 3. Remove the added line in the kube-apiserver config file OR copy from the backup 
