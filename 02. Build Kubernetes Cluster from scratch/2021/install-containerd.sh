#!/bin/bash

# Il doit avoir d'installer sur chaque noeud un Container runtime
# Containerd est aujourd'hui le plus utilisé et le plus léger
# Disable swapp
sudo swapoff -a
# Installation et configuraton des prerequis
## Chargement des modules necessaires pour containerd
cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

## kernel/sysctl parametres
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward = 1
EOF

## Apply sysctl sans reboot
sudo sysctl --system

# Installation de containerd
sudo apt-get update
sudo apt-get -y install containerd

# Configuration de containerd avec des valeurs par defaut et redemarrage du service
sudo mkdir -p /etc/containerd
containerd config default | sudo tee /etc/containerd/config.toml
sudo systemctl restart containerd

