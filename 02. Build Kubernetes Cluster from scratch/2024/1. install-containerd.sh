#------------------------------------------------------------
### Author : KOKI Jean-David - Senior Cloud & DevOps Engineer 
### This is the process of quickly Setting Up a Kubernetes Cluster 1.31 on an Ubuntu EC2 Machine with Kubeadm
### The cluster is composed of two Nodes - 1 Worker and 1 Master 
### Documentation : https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
### Happy Kuberneting :) 
#------------------------------------------------------------
# FIRST SCRIPT TO RUN ON ALL NODES 

#!/bin/bash

## 1. Disable swap

sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

## 2. Configuration & Containerd Installation (All nodes)

### Configure prerequisites
cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

sudo sysctl --system

### Install containerd
sudo apt-get update && sudo apt-get install -y containerd
sudo mkdir -p /etc/containerd

sudo containerd config default | sudo tee /etc/containerd/config.toml
sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml

sudo systemctl restart containerd