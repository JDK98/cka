# Create a Kubernetes v1.31 cluster on an AWS Ubuntu EC2 Machine with Kubeadm

## 1. Provision 2 AWS EC2 Ubuntu 24.04 Instance type t2.medium

## 2. Open Ports 

Doc Cilium : https://docs.cilium.io/en/stable/operations/system_requirements/#admin-system-reqs  

Doc Kubernetes : https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/

### FOR CONTROL PLANE (Master security group)

#### Required by Kubernetes

    Open 6443/tcp for All - Default Kubernetes API Server Port  

    Open 2379/tcp - 2380 only for the cluster - Default etcd port  

    Open 10250/tcp only for the cluster - Default kubelet port  

    Open 10259/tcp only for the cluster - Default Scheduler port   

    Open 10257/tcp only for the cluster - Default Controller port   

#### Required by Cilium CNI

    Open 8472/udp for control plane security group - Required by Cilium CNI  

    Open 8472/udp for worker security group - Required by Cilium CNI  

    Open 4240/tcp for control plane security group - Required by Cilium CNI  

    Open 4240/tcp for worker security group - Required by Cilium CNI  

    Open All ICMP - IPV4 for control plane security group - Required by Cilium CNI  

    Open All ICMP - IPV4 for worker security group - Required by Cilium CNI  

    Open 30000-32767 for control plane security group - Required for NodePort Services  

    Open 30000-32767 for worker security group - Required for NodePort Services  
    

### FOR WORKER NODES (Worker Security group)

#### Required by Kubernetes

    Open 10250 only for the cluster - Default Kubelet port  

    Open 10256 only for the cluster - Kube-Proxy  

    Open 30000-32767 for All - NodePort Services  

#### Required by Cilium CNI

    Open 8472/udp for control plane security group - Required by Cilium CNI  

    Open 8472/udp for worker security group - Required by Cilium CNI  

    Open 4240/tcp for control plane security group - Required by Cilium CNI  

    Open 4240/tcp for worker security group - Required by Cilium CNI  

    Open All ICMP - IPV4 for control plane security group - Required by Cilium CNI  

    Open All ICMP - IPV4 for worker security group - Required by Cilium CNI  

## 3. Install a CRI plugins on all Nodes ( Containerd )

    Run install-containerd.sh on All nodes 

## 4. Install Kubernetes Components 

    Run install-k8s-component.sh on All nodes 

## 5. Initialize the cluster 

    Run init-cluster.sh on Master Nodes 

## 6. Join the cluster 

    Check join-cluster.sh and run on worker Nodes 

## 7. Install CNI plugins on master node & test connectivity between pods 

    Check cilium.sh and run on master node

