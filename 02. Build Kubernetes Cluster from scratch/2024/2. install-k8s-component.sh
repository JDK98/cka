#------------------------------------------------------------
### Author : KOKI Jean-David - Senior Cloud & DevOps Engineer 
### This is the process of quickly Setting Up a Kubernetes Cluster on an Ubuntu EC2 Machine with Kubeadm
### The cluster is composed of two Nodes - 1 Worker and 1 Master 
### Documentation : https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
### Happy Kuberneting :) 
#------------------------------------------------------------
# SECOND SCRIPT TO RUN ON ALL NODES 

#!/bin/bash

## 1. Install dependency packages

sudo apt-get update && sudo apt-get install -y apt-transport-https ca-certificates curl gpg

## 2. Download the Google Cloud public signing key
### If the directory `/etc/apt/keyrings` does not exist, it should be created before the curl command
### sudo mkdir -p -m 755 /etc/apt/keyrings
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.31/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

## 3. Add Kubernetes to repository list
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.31/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list

## 4. Install kubelet, kubeadm & kubectl
sudo apt-get update
# apt-cache madison kubeadm (Check Kubeadm version - Only on manual execution)
### Install Kubernetes packages (Note: If you get a dpkg lock message, just wait a minute or two before trying the command again)
sudo apt-get install -y kubelet=1.31.0-1.1 kubeadm=1.31.0-1.1 kubectl=1.31.0-1.1
### Turn off automatic updates
sudo apt-mark hold kubelet kubeadm kubectl