#------------------------------------------------------------
### Author : KOKI Jean-David - Senior Cloud & DevOps Engineer 
### This is the process of quickly Setting Up a Kubernetes Cluster on an Ubuntu EC2 Machine with Kubeadm
### The cluster is composed of two Nodes - 1 Worker and 1 Master 
### Documentation : https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
### Happy Kuberneting :) 
#------------------------------------------------------------
# THIRD SCRIPT TO RUN ON CONTROL PLANE 

# Initialise le cluster
sudo kubeadm init

# Set kubectl access
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

