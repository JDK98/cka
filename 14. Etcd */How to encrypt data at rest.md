# Encrypt secret in etcd

1. Create secrets 

        Create secret1 and secret2
        Use secret1 as env and secret2 as mount file in the pod

2. Get the secrets inside etcd

```yaml
# The informations are written in plain text

sudo ETCDCTL_API=3 etcdctl --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/apiserver-etcd-client.crt --key=/etc/kubernetes/pki/apiserver-etcd-client.key get /registry/secrets/default/secret1 | secret2 
```

3. Encrypt

        # Api server don't work correctly for now
          Get more practice, Note & Post it 

