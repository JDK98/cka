# Readiness Probe

Readiness probe ensures that the container is in a state to receive requests
If a Readiness Probe fails the container keeps running but the pod will stay in a Not Ready state  
which means no new request from the API will be send to this pod 
(During application startup)

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: app-nginx
  labels:
    app: app-nginx
spec:
  containers:
  - name: app-nginx
    image: nginx
    ports:
    - containerPort: 8080
    readinessProbe:
      tcpSocket:
        port: 8080
      initialDelaySeconds: 10 #10s avant le premier check
      periodSeconds: 10 #Check chaque 1às après le premier check
    livenessProbe:
      tcpSocket:
        port: 8080
      initialDelaySeconds: 15
      periodSeconds: 20
```
